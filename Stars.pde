//星を描画し、キラキラを束ねるクラス

class Stars {

  float x, y, targX, targY;
  boolean ringF = false;
  ArrayList<Blinker> blinkers = new ArrayList<Blinker>();

  Stars(float _x, float _y, float _targX, float _targY) {

    targX = _targX;
    targY = _targY;
    x = _x;
    y = _y;
  }

  void disp() {
    noStroke();
    fill(random(100,255));
    ellipse(x, y, 5, 5);
    for (int i = blinkers.size() -1; i > 0; i--) {
      Blinker b = blinkers.get(i);
      b.disp();
      b.move();
      if (b.end()) {
        blinkers.remove(i);
      }
    }
  }

  void mover() {

    if (y > targY) {
      y -= 5;
      blinker();
    }
  }

  void blinker() {
    for (int i = 0; i<7; i++) {
      blinkers.add(new Blinker(x, y));
    }
  }
  
  boolean rin(){
    
    boolean r = false;
    if(y <= targY && !ringF){
      r = true;
      ringF = true;
    }
    return r;
  }
  
  float[] getPos(){
    
    return new float[] {x,y};
    
  }
  
}