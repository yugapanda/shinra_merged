import netP5.*;
import oscP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;

Solar_System SS;
int shuki = 1;
float counter = -2000;
float counterAdder = 0;
boolean chikyurekiF = false;
boolean starMusicF = false;
boolean siftF = false;
boolean movingF = true;
int starsFadeIn = 255;
int chikyurekiFadeIn = 255;


StarMaker sm = new StarMaker();
char[] keyset = {'1', 'Q', '2', 'W', '3', 'E', '4', 'R', '5', 'T', '6', 'Y', '7', 'U', '8', 'I', '9', 'O', '0', 'P'};

void setup() {

  fullScreen(FX2D);

  oscP5 = new OscP5(this, 12000);
  myRemoteLocation = new NetAddress("127.0.0.1", 12001);

  colorMode(HSB, 360, 255, 255);
  ellipseMode(CENTER);
  background(0);
  SS = new Solar_System();
  smooth();

  for (int i = 0; i<9; i++) {
    SS.turnOn(i);
  }
  noCursor();
}


void draw() {

  if (chikyurekiFadeIn < 255) {

    fill(0, 130);
    noStroke();
    rect(0, 0, width, height);
    strokeWeight(0.1);
    pushMatrix();
    translate(width/2, height/2);
    rotate(radians(30));
    stroke(255);
    if (chikyurekiF) {
      line(-width, 0, width, 0);
      line(0, -height, 0, height);
    }
    noStroke();
    popMatrix();
    SS.drawPlan(int(counter));

    if (movingF && frameCount % shuki == 0)counter += counterAdder;

    fill(0, chikyurekiFadeIn);
    noStroke();
    rect(0, 0, width, height);
  }

  if (starsFadeIn < 255) {
    background(0);
    sm.disp();
    fill(0, starsFadeIn);
    rect(0, 0, width, height);
  }

  if (!chikyurekiF & !starMusicF) {
    if(chikyurekiFadeIn < 255)chikyurekiFadeIn++;
    if(starsFadeIn < 255)starsFadeIn++;
  }

  if (!chikyurekiF && chikyurekiFadeIn < 255) {
    chikyurekiFadeIn++;
  } else if (chikyurekiF && chikyurekiFadeIn > 0) {
    chikyurekiFadeIn--;
  }
  if (!starMusicF && starsFadeIn < 255) {
    starsFadeIn++;
  } else if (starMusicF && starsFadeIn > 0) {
    starsFadeIn--;
  }
  //text(counter,width- 100, 100);
}


void keyPressed() {

  if (key == ' ' & movingF) {
    movingF = false;
  } else if (key == ' ' & !movingF) {
    movingF = true;
  }
  if (key == 'Z' & !chikyurekiF) {
    chikyurekiF = true;
  } else if (key == 'Z' & chikyurekiF) {
    chikyurekiF = false;
  }

  if (key == 'X' & !starMusicF) {

    starMusicF = true;
  } else if (key == 'X' & starMusicF) {

    starMusicF = false;
  }

  if (starMusicF) {
    for (int i = 0; i<20; i++) {
      if (key == keyset[i])sm.addStar((width/20)*(1+i), height);
    }
  }

  if (keyCode == 38 & chikyurekiF & !siftF) {
    counterAdder += 0.1;
  } else if (keyCode == 38 & chikyurekiF & siftF) {
    counterAdder += 1;
  }

  if (keyCode == 40 & chikyurekiF & !siftF) {
    counterAdder -= 0.1;
  } else if (keyCode == 40 & chikyurekiF & siftF) {
    counterAdder -= 1;
  }

  if (keyCode == 16 & !siftF) {
    siftF = true;
  }
  if (keyCode == 39 && starMusicF) {
    sm.SMM.chageTonal(2);
  }
  if (keyCode == 37 && starMusicF) {
    sm.SMM.chageTonal(-2);
  }
}


void keyReleased() {

  if (keyCode == 16 & siftF) {
    siftF = false;
  }
}