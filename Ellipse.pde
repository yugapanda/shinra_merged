/*
広がっていく円を発生させるクラス

*/

class Ellipser{
  
  float x,y;
  int size = 0;
  int a = 255;
  Ellipser(float _x, float _y){
    x = _x;
    y = _y;
    
  }
  
  void disp(){
    noFill();
    stroke(255,a);
    ellipse(x,y,size,size);
    
    size+=1;
    a--;
    
  }
  
  boolean end(){
    
    boolean e = false;;
    if(a < 0){
      
      e = true;
    }
    return e;
  }
  
}