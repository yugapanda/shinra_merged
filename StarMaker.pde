//StarMusic packageの、星を束ねるクラス

class StarMaker {

  ArrayList<Stars> stars = new ArrayList<Stars>();
  ArrayList<Blinker> blinker = new ArrayList<Blinker>();
  StarMelodyMaker SMM = new StarMelodyMaker();


  StarMaker() {
  }

  void disp() {

    for (Stars s : stars) {

      s.disp();
      s.mover();
      if (s.rin()) {
        ArrayList<Integer> n = new ArrayList<Integer>();
        float[] x = s.getPos();
        n.add(int(x[1]));
        int note = SMM.getNote(int(x[1]),40,SMM.tonal);
        ArrayList<Integer> noteList = new ArrayList<Integer>();
        noteList.add(note);
        SMM.sendOSC(noteList);
      }
    }

    ArrayList<Integer> notes = SMM.makeMusic(stars);
    SMM.sendOSC(notes);

    ArrayList<Integer[]> timeLinePos = SMM.getOnTimeLineStar(stars);

    for (Integer[] t : timeLinePos) {
      for (int i = 0; i<10; i++) {
        blinker.add(new Blinker(t[0].intValue(), t[1].intValue()));
      }
    }

    for (int i = blinker.size() - 1; i>0; i--) {

      blinker.get(i).disp();
      blinker.get(i).move();

      if (blinker.get(i).end()) {
        blinker.remove(i);
      }
    }


    SMM.timeLineInc();
  }


  void addStar(float _x, float _y) {

    stars.add(new Stars(_x, _y, _x, random(height)));
  }
}