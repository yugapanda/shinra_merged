//キラキラを束ねるクラス

class Blinker {
  float x, y, addx, addy;
  int life;
  Blinker(float _x, float _y) {
    x = _x;
    y = _y;
    addx = random(-2, 2);
    addy = random(3);
    life = 255;
  }

  void disp() {
    stroke(random(200, 255), life);
    line(x + random(-5), y, x + random(5), y);
    line(x, y + random(-5), x, y + random(5));
    noStroke();
    fill(170, 255, random(100, 255));
    ellipse(x, y, random(2), random(2));
    if (life < 200 && random(100) < 1) {
      float liner = random(20);
      stroke(255, life);
      line(x - liner, y - liner, x + liner, y + liner);
      line(x + liner, y - liner, x - liner, y + liner);
    }
    noStroke();
  }
  void move() {

    x += addx;
    y += addy;
    life-=5;
  }
  boolean end() {
    boolean e = false;
    if (life <= 0) {

      e = true;
    }

    return e;
  }
}