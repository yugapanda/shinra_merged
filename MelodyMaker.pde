/*
地球暦の自転を使ってメロディを作るクラス
*/

class MelodyMaker {

  int oct = 4;
  int skipProb = 10;
  int initValue;
  int oldNote;

  MelodyMaker(int _oct) {

    oct = _oct;
    initValue = oct;
  }



  int makeNote(int _tonal) {

    int note = howSkipping(stepOrSkip(), upOrDown());
    int next = oldNote + note;

    if (next > 6) {
      oct++;
      next = next - 6;
    } else if (next < 0) {
      oct--;
      next = next + 6;
    }
    oldNote = next;
    int nextNote = tonalScale(_tonal)[next] + oct*7;
    if((initValue + 3)*7 < oct + nextNote || (initValue - 3)*7 > nextNote){
     oct = initValue; 
    }
    return nextNote;
  }

  boolean stepOrSkip() {
    boolean s = false;
    if (random(skipProb) < 1) {
      s = true;
      skipProb = int(random(3, 20));
    } else if (skipProb > 3) {
      skipProb--;
    }

    return s;
  }

  boolean upOrDown() {
    boolean u = false;

    if (random(10) >= 5)u = true; 

    return u;
  }

  int howSkipping(boolean _sos, boolean _uod) {

    int skipVal = 1;
    if (_sos) {
      boolean looper = true;
      while (looper) {

        skipVal++;
        if (skipVal > 5) {
          looper = false;
        }
        if (random(2) <= 1) {
          looper = false;
        }
      }
    }

    if (!_uod) {
      skipVal *= -1;
    }



    return skipVal;
  }

  int[] tonalScale(int _tonal) {


    int m[] = new int[7];


    m[0] = _tonal;
    m[1] = _tonal + 2;
    m[2] = _tonal + 4;
    m[3] = _tonal + 6;
    m[4] = _tonal + 7;
    m[5] = _tonal + 9;
    m[6] = _tonal + 11;


    return m;
  }
}