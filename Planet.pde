/*

Solar_Systemに必要な、星を表示したり情報を保持したりするクラス

*/

class Planet {

  int num;//固有のナンバー
  float revolution;//公転周期
  int rotation;//自転周期
  int size;//大きさ
  int distance;//太陽からの距離
  float x;//現在地
  float y;//現在地
  boolean[] bingo = new boolean[8];//どこの惑星と今傾きが同じか
  float col;//色
  boolean onOrOff;
  int revoInit;
  String name;


  Planet(String _name,int _n, int _rev, int _rot, int _size, int _dist, float _col,int _revoInit) {
    name = _name;
    num = _n;
    revolution = _rev;
    rotation = _rot;
    size = _size;
    distance = _dist;
    col = _col;
    revoInit = _revoInit;
  }


  void drawPlanet(float now) {


    noFill();
    strokeWeight(0.1);
    stroke(255);
    ellipse(width/2, height/2, distance*2, distance*2);
    
    strokeWeight(1);

    float rev = 360/revolution;//公転周期で360を割って、1周に必要な時間を出す

    x = cos(radians(now/24*rev + revoInit))*distance + width/2;
    y = sin(radians(now/24*rev + revoInit))*distance + height/2;


    noStroke();
    fill(col, 255, 255);
    text(name,x - 20,y - 25);
    ellipse(x, y, size, size);


    //中で動いている色
    /*
    for (int i = 0; i<50; i++) {
     
     float ranx = random(x - 10, x + 10);
     float rany = random(y - 10, y + 10);
     fill(random(col-10,col+10), 255, random(100,255),180);
     ellipse(ranx, rany, random(5), random(5));
     }
     */
  }

  float getHowRotation(float _now) {

    float r = _now % float(rotation);

    return r;
  }

  int getRotation() {

    return rotation;
  }

  float getAngle() {

    float m = (height/2 - y) / (width/2 - x);

    return m;
  }

  void setSameAngle(int _num) {
    bingo[_num] = true;
  }

  void clearSameAngle() {

    for (int i = 0; i< bingo.length; i++) {
      bingo[i] = false;
    }
  }


  boolean[] getSameAngle() {

    return bingo;
  }

  float[] getPos() {

    float[] pos = {x, y };

    return pos;
  }

  boolean isOn() {

    return onOrOff;
  }

  void turnOnOrOff() {

    if (onOrOff) {
      onOrOff = false;
    } else {
      onOrOff = true;
    }
  }

  void turnOff() {

    onOrOff = false;
  }
  int getNum(){
    
   return num; 
  }
}