/*

メインでOSCを読み込んでおく必要がある

*/

class StarMelodyMaker {

  int timeLine = 0;
  int tonal = 0;
  StarMelodyMaker() {
  }

  void sendOSC(ArrayList<Integer> list) {

    for (int i : list) {
      OscMessage notes = new OscMessage("/note");
      notes.add(i);
      oscP5.send(notes, myRemoteLocation);
    }
  }

  void timeLineInc() {

    timeLine+=5;
    if(timeLine > width)timeLine = 0;
  }


  ArrayList<Integer> makeMusic(ArrayList<Stars> stars) {


    ArrayList<Integer> notes = new ArrayList<Integer>();
    for (Stars s : stars) {

      int sXPos = int(s.getPos()[0]);
      int sYPos = int(s.getPos()[1]);

      if (isOnTimeLine(timeLine, 5, false, sXPos)) {
        notes.add(getNote(sYPos, 40, tonal));
      }
    }

    return notes;
  }

  ArrayList<Integer[]> getOnTimeLineStar(ArrayList<Stars> stars) {
    ArrayList<Integer[]> notes = new ArrayList<Integer[]>();
    for (Stars s : stars) {

      int sXPos = int(s.getPos()[0]);
      int sYPos = int(s.getPos()[1]);

      if (isOnTimeLine(timeLine, 5, false, sXPos)) {
        Integer[] pos = {sXPos,sYPos};
        notes.add(pos);
      }
    }
    return notes;
  }

  boolean isOnTimeLine(int _nowTimeLine, int _interval, boolean _visual, int _xPos) {

    /*
    タイムラインの位置、タイムラインのインターバル、可視か不可視化か、xPosを入力すると、タイムライン上に星があるかどうかをboolean型で返す
     */
    boolean exist = false;
    if (_visual) {
      stroke(255);
      line(_nowTimeLine, 0, _nowTimeLine, height);
    }
    if (_xPos >= _nowTimeLine - _interval & _xPos < _nowTimeLine) {
      exist = true;
    }
    return exist;
  }


  int getNote(int _pos, int _step, int _tonal) {

    /*
    starのyPosと、画面を何分割するか、そして調性を入れると、midiナンバーを出力する
     
     */
    int[] note =  new int[2];
    int noteDate = (height - _pos) / _step;
    note[0] = noteDate % 7;
    note[1] = noteDate / 7;

    int value = note[1]*12 + tonalScale(_tonal)[note[0]];


    return value;
  }


  int[] tonalScale(int _tonal) {


    int m[] = new int[7];


    m[0] = _tonal;
    m[1] = _tonal + 2;
    m[2] = _tonal + 4;
    m[3] = _tonal + 6;
    m[4] = _tonal + 7;
    m[5] = _tonal + 9;
    m[6] = _tonal + 11;
    return m;
  }
  
  void chageTonal(int c){
    tonal += c;
  }
}