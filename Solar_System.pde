/*
Planetを束ねるクラス。
*/

class Solar_System {

  HashMap<String, Integer> planetsName = new HashMap<String, Integer>();
  ArrayList<Ellipser> ep = new ArrayList<Ellipser>();
  float[][] back = new float[1000][3];
  int suisei = 50;
  int kinsei = 100;
  int chikyu = 150;
  int kasei =  200;
  int mokusei = 250;
  int dosei =  300;
  int tensei = 350;
  int kaisei = 400;
  int globalTonal = 0;

  Planet[] planets = new Planet[9];
  MelodyMaker[] MM = new MelodyMaker[9];

  int sun = 1;


  Solar_System() {

    planets[0] = new Planet("水星",0, 87, 58*24*10, 24, 100, 30, 90);//水
    planets[1] = new Planet("金星",1, 224, 243*24*10, 24, 150, 85, 230);//金
    planets[2] = new Planet("地球",2, 365, 24*10, 30, 200, 210, 100);//地
    planets[3] = new Planet("火星",3, 686, 25*10, 24, 250, 15, 130);//火
    planets[4] = new Planet("木星",4, 11*365, 9*10, 24, 300, 10, 280);//木
    planets[5] = new Planet("土星",5, 29*365, 10*10, 24, 350, 150, 200);//土
    planets[6] = new Planet("天王星",6, 84*365, 17*10, 24, 400, 180, 230);//天
    planets[7] = new Planet("海王星",7, 164*365, 16*10, 24, 450, 230, 140);//海
    planets[8] = new Planet("太陽",8, 164*365, 25*24*10, 24, 0, 0, 0);//太陽

    MM[0] = new MelodyMaker(8);
    MM[1] = new MelodyMaker(8);
    MM[2] = new MelodyMaker(8);
    MM[3] = new MelodyMaker(8);
    MM[4] = new MelodyMaker(9);
    MM[5] = new MelodyMaker(10);
    MM[6] = new MelodyMaker(11);
    MM[7] = new MelodyMaker(8);
    MM[8] = new MelodyMaker(8);

    for (float[] b : back) {
      b[0] = random(width);
      b[1] = random(width);
      b[2] = random(5);
    }
  }

  void drawPlan(int _counter) {

    noFill();
    stroke(255);

    if (frameCount % 2000 == 1) {
      for (float[] b : back) {
        b[0] = random(width);
        b[1] = random(width);
        b[2] = random(5);
      }
    }

    for (float[] b : back) {
      fill(random(255), random(255));
      noStroke();
      ellipse(b[0], b[1], b[2], b[2]);
    }

    for (Planet p : planets) {
      p.clearSameAngle();
      if (p.isOn()) {


        p.drawPlanet(_counter);

        /*
        
         if (p.getHowRotation(_counter) == 0 && frameCount % shuki == 0) {
         println(p.getNum() +" : "+p.getHowRotation(_counter));
         OscMessage notes = new OscMessage("/note"+p.getNum());
         notes.add(MM[p.getNum()].makeNote(globalTonal));
         oscP5.send(notes, myRemoteLocation); 
         ep.add(new Ellipser(p.getPos()[0], p.getPos()[1]));
         }
         
         */
      }
      for (int i = ep.size() - 1; i>0; i--) {

        ep.get(i).disp();
        if (ep.get(i).end()) {
          ep.remove(i);
        }
      }


      /*傾きのシステム
       float m = p.getAngle();
       
       for (Planet pp : planets) {
       
       if (pp.num != p.num && m - 0.01 < pp.getAngle() & m + 0.01 > pp.getAngle()) {
       
       p.setSameAngle(pp.num);
       
       }
       }
       */
    }
  }

  void turnOn(int num) {

    planets[num].turnOnOrOff();
  }
  void allOff() {

    for (int i = 0; i<planets.length; i++) {
      planets[i].turnOff();
    }
  }
}